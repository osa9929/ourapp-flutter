// import 'package:ourapp/ipconfig.dart';
import 'dart:io';

import 'package:ourapp/screens/accountManagementPage.dart';
import 'package:ourapp/screens/acceptedRquestsPage.dart';
import 'package:ourapp/screens/evaluationPage.dart';
import 'package:ourapp/screens/examRequest.dart';
import 'package:ourapp/screens/loginpage.dart';
import 'package:ourapp/screens/myPreviousOrders.dart';
import 'package:ourapp/screens/recitationRequest.dart';
import 'package:ourapp/screens/registerPage.dart';
import 'package:ourapp/screens/studentHomePage.dart';
import 'package:flutter/material.dart';
import 'package:ourapp/screens/teacherHomePage.dart';
import 'package:ourapp/screens/unActivatedPaeg.dart';
import 'package:ourapp/screens/updateScreen.dart';

void main() {
  HttpOverrides.global = MyHttpOverrides();
  runApp(const MyApp());
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'الحفاظ',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:
          // const UpdateScreen(),
          const LoginPage(),
      routes: {
        "LoginPage": (context) => const LoginPage(),
        "RegisterPage": (context) => const RegisterPage(),
        "UnActivatedPage": (context) => const UnActivatedPage(),
        "StudentHomePage": (context) => const StudentHomePage(),
        "RecitationRequestPage": (context) => const RecitationRequestPage(),
        "ExamRquestPage": (context) => const ExamRquestPage(),
        "TeacherHomePage": (context) => const TeacherHomePage(),
        "AcceptedRequestsPage": (context) => const AcceptedRequestsPage(),
        "EvaluationPage": (context) => const EvaluationPage(),
        "MyPreviousOrdersPage": (context) => const MyPreviousOrdersPage(),
        "AccountManagementPage": (context) => const AccountManagementPage(),
      },
    );
  }
}
