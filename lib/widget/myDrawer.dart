import 'package:flutter/material.dart';
import 'package:ourapp/static/userinfo.dart';

import 'logOutDrawerWidget.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: ListView(children: [
          const ListTile(
              title: Text(
                "الخيارات",
                style: TextStyle(color: Colors.white, fontSize: 30),
              ),
              leading: SizedBox(),
              tileColor: Colors.blue),
          UserIfo.data!.role != "student"
              ? ListTile(
                  title: const Text("الطلبات المستلمة"),
                  leading: const Icon(Icons.save_alt_outlined),
                  onTap: () {
                    Navigator.pushNamed(context, "AcceptedRequestsPage");
                  },
                )
              : const SizedBox(),
          UserIfo.data!.role != "student"
              ? ListTile(
                  title: const Text("إرسال طلب"),
                  leading: const Icon(Icons.send),
                  onTap: () {
                    Navigator.pushNamed(context, "StudentHomePage");
                  },
                )
              : const SizedBox(),
          ListTile(
            title: const Text("طلباتي السابقة"),
            leading: const Icon(Icons.save),
            onTap: () {
              Navigator.pushNamed(context, "MyPreviousOrdersPage");
            },
          ),
          const Divider(
            thickness: 1,
            indent: 10,
            endIndent: 10,
          ),
          ListTile(
            title: const Text("إدارة الحساب"),
            leading: const Icon(Icons.settings),
            onTap: () {
              Navigator.pushNamed(context, "AccountManagementPage");
            },
          ),
          const LogOutDrawerWidget()
        ]),
      ),
    );
  }
}
