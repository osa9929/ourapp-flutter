import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../api/requests.dart';

class LogOutDrawerWidget extends StatefulWidget {
  const LogOutDrawerWidget({Key? key}) : super(key: key);

  @override
  State<LogOutDrawerWidget> createState() => _LogOutDrawerWidgetState();
}

class _LogOutDrawerWidgetState extends State<LogOutDrawerWidget> {
  late final SharedPreferences prefs;
  bool tryLogOut = false;
  Future<void> logoutCommit() async {
    try {
      setState(
        () {
          tryLogOut = true;
        },
      );

      print('try logout ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: "api/logout",
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 200) {
        prefs = await SharedPreferences.getInstance();
        prefs.remove('token');
        Navigator.pushNamedAndRemoveUntil(
            context, "LoginPage", (route) => false);
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        tryLogOut = false;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: tryLogOut
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : const Text("تسجيل الخروج"),
      leading: const Icon(Icons.logout),
      onTap: () {
        logoutCommit();
      },
    );
  }
}
