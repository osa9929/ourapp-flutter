import 'package:flutter/material.dart';
import 'package:ourapp/api/models/orderapi.dart';
import 'package:http/http.dart' as http;
import 'package:ourapp/screens/editRecitationRequest.dart';

import '../api/requests.dart';

class MyRequestCard extends StatefulWidget {
  const MyRequestCard({Key? key, required this.order}) : super(key: key);
  final Orderapi order;

  @override
  State<MyRequestCard> createState() => _MyRequestCardState();
}

class _MyRequestCardState extends State<MyRequestCard> {
  bool tryDelete = false;

  Future<void> deleteMyOrder() async {
    setState(() {
      tryDelete = true;
    });
    try {
      print('try delete ///////////////////////////');
      http.Response response = await Requests.deleteRequest(
        urlRout: "api/order/delete/${widget.order.id}",
      );
      print('Response status: ${response.statusCode}');
      print('exam Response body: ${response.body}');
      if (response.statusCode == 200) {
        Navigator.of(context).pushReplacementNamed("StudentHomePage");
      }
    } catch (e) {
      print(e);
    }
    setState(() {
      tryDelete = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      decoration: BoxDecoration(
          color: widget.order.assignedTo == null
              ? Colors.grey[300]
              : Colors.greenAccent[100],
          borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          cardHeader(),
          cardBody(),
        ],
      ),
    );
  }

  Widget cardHeader() => Column(
        children: [
          Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Material(
                  child: IconButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => EditRecitationRequest(
                                  order: widget.order,
                                )),
                      );
                    },
                    icon: const Icon(
                      Icons.edit,
                      color: Colors.blue,
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    widget.order.reading == 1
                        ? "طلب التسميع الحالي (اقراء)"
                        : "طلب التسميع الحالي",
                    style: const TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Material(
                  child: tryDelete
                      ? const CircularProgressIndicator()
                      : IconButton(
                          onPressed: () {
                            deleteMyOrder();
                          },
                          icon: const Icon(
                            Icons.delete,
                            color: Colors.red,
                          ),
                        ),
                ),
              ),
            ],
          ),
          const Divider(
            thickness: 1,
          ),
        ],
      );

  Widget cardBody() => Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: [
                cell(head: "الجزء", body: widget.order.shapterNum.toString()),
                const SizedBox(width: 5),
                cell(head: "من الصفحة", body: widget.order.fromPage.toString()),
                const SizedBox(width: 5),
                cell(head: "إلى الصفحة", body: widget.order.toPage.toString())
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: [
                cell(
                    head: "التاريخ",
                    body: widget.order.orderDate!.year.toString() +
                        "/${widget.order.orderDate!.month.toString()}" +
                        "/${widget.order.orderDate!.day.toString()}"),
                const SizedBox(width: 5),
                cell(head: "الوقت", body: widget.order.orderTime.toString())
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Row(
              children: [
                cell(
                    head: "الأستاذ",
                    body: widget.order.teacherName == null
                        ? "لم يتحدد بعد"
                        : widget.order.teacherName.toString(),
                    color: Colors.green[600])
              ],
            ),
          ),
        ],
      );

  Widget cell(
          {String head = "head",
          String body = "body",
          Color? color = Colors.blue}) =>
      Expanded(
          child: Container(
        padding: const EdgeInsets.all(10),
        // height: 30,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5), color: Colors.white),
        child: Center(
          child: Column(children: [
            FittedBox(child: Text(head)),
            const SizedBox(height: 5),
            SizedBox(
              height: 30,
              child: FittedBox(
                child: Text(
                  body,
                  style: TextStyle(
                    color: color,
                  ),
                ),
              ),
            )
          ]),
        ),
      ));
}
