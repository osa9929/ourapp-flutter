import 'package:flutter/material.dart';
import 'package:ourapp/api/models/incommingRequestsapi.dart';
import 'package:http/http.dart' as http;
import '../api/requests.dart';

class PreviousOrderCard extends StatefulWidget {
  const PreviousOrderCard(
      {Key? key, required this.order, this.showButton = true})
      : super(key: key);
  final Exam order;
  final bool showButton;

  @override
  State<PreviousOrderCard> createState() => _PreviousOrderCardState();
}

class _PreviousOrderCardState extends State<PreviousOrderCard> {
  bool trySendRquest = false;
  bool isDeleted = false;

  Future<bool> deleteOrder() async {
    try {
      print('try  ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: widget.order.fromPage == null && widget.order.toPage == null
            ? "api/teacher/acceptExam/${widget.order.id}"
            : "api/teacher/acceptOrder/${widget.order.id}",
      );
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 201) {
        return true;
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  Future<void> deleteOrderResult() async {
    setState(
      () {
        trySendRquest = true;
      },
    );
    bool isDone = await deleteOrder();

    if (isDone == true) {
      // Navigator.pushReplacementNamed(context, "TeacherHomePage");
      setState(
        () {
          trySendRquest = false;
          isDeleted = true;
        },
      );
    } else {
      setState(
        () {
          trySendRquest = false;
        },
      );
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشلت العملية أعد المحاولة',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(milliseconds: 1500),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  bool isToday(DateTime date) {
    DateTime toDay = DateTime.now();
    if (date.year == toDay.year &&
        date.month == toDay.month &&
        date.day == toDay.day) {
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: widget.order.status == "Accepted"
              ? Colors.greenAccent[100]
              : widget.order.status == "Refused"
                  ? Colors.red[100]
                  : Colors.grey[300],
          //  Colors.grey[300],
          borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          cardHeader(),
          cardBody(),
          widget.order.note == "" ||
                  widget.order.note == "null" ||
                  widget.order.note == null
              ? const SizedBox()
              : cardFooter()
        ],
      ),
    );
  }

  Widget cardHeader() => Column(
        children: [
          Row(
            children: [
              const Expanded(child: Center(child: Text("اسم الأستاذ"))),
              Expanded(
                child: Center(
                  child: Text(
                    widget.order.teacherName.toString(),
                    style: const TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              const Expanded(
                child: Center(
                  child: Text("التقييم النسبي"),
                ),
              ),
              Expanded(
                child: Center(
                  child: Text(
                    "%" + widget.order.rate.toString(),
                    style: const TextStyle(
                        color: Colors.blue,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
          const Divider(
            thickness: 1,
          ),
        ],
      );

  Widget cardBody() => Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            child: Row(
              children: [
                cell(head: "الجزء", body: widget.order.shapterNum.toString()),
                // const SizedBox(width: 5),
                widget.order.fromPage != null
                    ? cell(
                        head: "من الصفحة",
                        body: widget.order.fromPage.toString())
                    : const SizedBox(),
                // const SizedBox(width: 5),
                widget.order.toPage != null
                    ? cell(
                        head: "إلى الصفحة",
                        body: widget.order.toPage.toString())
                    : const SizedBox(),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            child: Row(
              children: [
                cell(
                    head: "التاريخ",
                    body:
                        "${widget.order.orderDate!.year}/${widget.order.orderDate!.month}/${widget.order.orderDate!.day}"),
                //const SizedBox(width: 5),
                cell(head: "الوقت", body: "${widget.order.orderTime}")
              ],
            ),
          ),
        ],
      );

  Widget cardFooter() => Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: Container(
            width: double.maxFinite,
            padding: EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
            ),
            child: Column(
              children: [
                Row(
                  children: const [
                    Text("ملاحظة:"),
                  ],
                ),
                Text(
                  widget.order.note.toString(),
                ),
              ],
            ),
          ),
        ),
      );

  Widget cell(
          {String head = "head",
          String body = "body",
          Color? color = Colors.blue}) =>
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 2),
          child: Container(
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: Colors.white),
            child: Center(
              child: Column(
                children: [
                  Text(head),
                  const SizedBox(height: 5),
                  SizedBox(
                    height: 25,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        body,
                        style: TextStyle(
                          color: color,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
