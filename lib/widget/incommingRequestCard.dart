import 'package:flutter/material.dart';
import 'package:ourapp/api/models/incommingRequestsapi.dart';
import 'package:http/http.dart' as http;

import '../api/requests.dart';

class IncommingRequsetCard extends StatefulWidget {
  const IncommingRequsetCard({Key? key, required this.order}) : super(key: key);
  final Exam order;

  @override
  State<IncommingRequsetCard> createState() => _IncommingRequsetCardState();
}

class _IncommingRequsetCardState extends State<IncommingRequsetCard> {
  bool trySendRquest = false;
  bool isAccepted = false;

  Future<bool> acceptOrder() async {
    try {
      print('try  ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: widget.order.fromPage == null && widget.order.toPage == null
            ? "api/teacher/acceptExam/${widget.order.id}"
            : "api/teacher/acceptOrder/${widget.order.id}",
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 201) {
        return true;
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  Future<void> acceptOrderResult() async {
    setState(
      () {
        trySendRquest = true;
      },
    );
    bool isDone = await acceptOrder();

    if (isDone == true) {
      // Navigator.pushReplacementNamed(context, "TeacherHomePage");
      setState(
        () {
          trySendRquest = false;
          isAccepted = true;
        },
      );
    } else {
      setState(
        () {
          trySendRquest = false;
        },
      );
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشلت العملية أعد المحاولة',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(milliseconds: 1500),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: Colors.grey[300], borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          cardHeader(),
          cardBody(),
          cardFooter(),
        ],
      ),
    );
  }

  Widget cardHeader() => Column(
        children: [
          widget.order.ejaza == 1
              ? const Center(
                  child: Text(
                    "(إجازة)",
                    style: TextStyle(color: Colors.deepOrange, fontSize: 25),
                  ),
                )
              : widget.order.reading == 1
                  ? const Center(
                      child: Text(
                        "(إقرآء)",
                        style: TextStyle(color: Colors.green, fontSize: 25),
                      ),
                    )
                  : const SizedBox(),
          Row(
            children: [
              const Expanded(child: Center(child: Text("اسم الطالب"))),
              Expanded(
                child: Center(
                  child: Text(
                    widget.order.studentName.toString(),
                    style: const TextStyle(
                        color: Colors.blue, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
          // Row(
          //   children: [
          //     const Expanded(child: Center(child: Text("نوع الطلب"))),
          //     Expanded(
          //       child: Center(
          //         child: Text(
          //           "تسميع",
          //           style: TextStyle(
          //               color: Colors.blue, fontWeight: FontWeight.bold),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          const Divider(
            thickness: 1,
          ),
        ],
      );

  Widget cardBody() => Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            child: Row(
              children: [
                cell(head: "الجزء", body: widget.order.shapterNum.toString()),
                // const SizedBox(width: 5),
                widget.order.fromPage != null
                    ? cell(
                        head: "من الصفحة",
                        body: widget.order.fromPage.toString())
                    : const SizedBox(),
                // const SizedBox(width: 5),
                widget.order.toPage != null
                    ? cell(
                        head: "إلى الصفحة",
                        body: widget.order.toPage.toString())
                    : const SizedBox(),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 2),
            child: Row(
              children: [
                cell(
                    head: "التاريخ",
                    body:
                        "${widget.order.orderDate!.year}/${widget.order.orderDate!.month}/${widget.order.orderDate!.day}"),
                //const SizedBox(width: 5),
                cell(head: "الوقت", body: "${widget.order.orderTime}")
              ],
            ),
          ),
        ],
      );

  Widget cardFooter() => Center(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          child: trySendRquest
              ? const CircularProgressIndicator()
              : isAccepted
                  ? const Text(
                      "تم الاستلام",
                      style: TextStyle(fontSize: 20, color: Colors.green),
                    )
                  : ElevatedButton(
                      child: const Text("استلام الطلب"),
                      onPressed: () {
                        acceptOrderResult();
                      },
                    ),
        ),
      );

  Widget cell(
          {String head = "head",
          String body = "body",
          Color? color = Colors.blue}) =>
      Expanded(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 2),
          child: Container(
            padding: const EdgeInsets.all(0),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5), color: Colors.white),
            child: Center(
              child: Column(
                children: [
                  Text(head),
                  const SizedBox(height: 5),
                  SizedBox(
                    height: 25,
                    child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text(
                        body,
                        style: TextStyle(
                          color: color,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
