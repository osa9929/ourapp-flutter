// To parse this JSON data, do
//
//     final loginapi = loginapiFromJson(jsonString);

import 'dart:convert';

Loginapi loginapiFromJson(String str) => Loginapi.fromJson(json.decode(str));

User userFromJson(String str) => User.fromJson(json.decode(str));

String loginapiToJson(Loginapi data) => json.encode(data.toJson());

class Loginapi {
  Loginapi({
    this.user,
    required this.token,
  });

  User? user;
  String token;

  factory Loginapi.fromJson(Map<String, dynamic> json) => Loginapi(
        user: User.fromJson(json["user"]),
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "user": user!.toJson(),
        "token": token,
      };
}

class User {
  User({
    this.id,
    this.name,
    this.userClass,
    this.phone,
    this.group,
    this.role,
    this.activation,
    this.allowShapter,
    this.deletedAt,
    this.createdAt,
    this.updatedAt,
  });

  int? id;
  String? name;
  String? userClass;
  int? phone;
  String? group;
  String? role;
  int? activation;
  String? allowShapter;
  dynamic deletedAt;
  DateTime? createdAt;
  DateTime? updatedAt;

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json["id"],
        name: json["name"],
        userClass: json["class"],
        phone: json["phone"],
        group: json["group"],
        role: json["role"],
        activation: json["activation"],
        allowShapter: json["allow_shapter"],
        deletedAt: json["deleted_at"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "class": userClass,
        "phone": phone,
        "group": group,
        "role": role,
        "activation": activation,
        "allow_shapter": allowShapter,
        "deleted_at": deletedAt,
        "created_at": createdAt!.toIso8601String(),
        "updated_at": updatedAt!.toIso8601String(),
      };
}
