// To parse this JSON data, do
//
//     final examapi = examapiFromJson(jsonString);

import 'dart:convert';

Examapi examapiFromJson(String str) => Examapi.fromJson(json.decode(str));

String examapiToJson(Examapi data) => json.encode(data.toJson());

class Examapi {
  Examapi({
    this.id,
    this.userId,
    this.shapterNum,
    this.ejaza,
    this.status,
    this.assignedTo,
    this.note,
    this.rate,
    this.orderTime,
    this.orderDate,
    this.teacherName,
  });

  int? id;
  int? userId;
  int? shapterNum;
  int? ejaza;
  dynamic status;
  dynamic assignedTo;
  dynamic note;
  dynamic rate;
  String? orderTime;
  DateTime? orderDate;
  dynamic teacherName;

  factory Examapi.fromJson(Map<String, dynamic> json) => Examapi(
        id: json["id"],
        userId: json["user_id"],
        shapterNum: json["shapter_num"],
        ejaza: json["ejaza"],
        status: json["status"],
        assignedTo: json["assigned_to"],
        note: json["note"],
        rate: json["rate"],
        orderTime: json["order_time"],
        orderDate: DateTime.parse(json["order_date"]),
        teacherName: json["teacher_name"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "shapter_num": shapterNum,
        "ejaza": ejaza,
        "status": status,
        "assigned_to": assignedTo,
        "note": note,
        "rate": rate,
        "order_time": orderTime,
        "order_date": orderDate!.toIso8601String(),
        "teacher_name": teacherName,
      };
}
