// To parse this JSON data, do
//
//     final incommingRequestsapi = incommingRequestsapiFromJson(jsonString);

import 'dart:convert';

IncommingRequestsapi incommingRequestsapiFromJson(String str) =>
    IncommingRequestsapi.fromJson(json.decode(str));

String incommingRequestsapiToJson(IncommingRequestsapi data) =>
    json.encode(data.toJson());

class IncommingRequestsapi {
  IncommingRequestsapi({
    this.orders,
    this.exams,
  });

  List<Exam>? orders;
  List<Exam>? exams;

  factory IncommingRequestsapi.fromJson(Map<String, dynamic> json) =>
      IncommingRequestsapi(
        orders: List<Exam>.from(json["orders"]
            .map((x) => Exam.fromJson(x as Map<String, dynamic>))),
        exams: List<Exam>.from(
            json["exams"].map((x) => Exam.fromJson(x as Map<String, dynamic>))),
      );

  Map<String, dynamic> toJson() => {
        "orders": List<dynamic>.from(orders!.map((x) => x.toJson())),
        "exams": List<dynamic>.from(exams!.map((x) => x.toJson())),
      };
}

class Exam {
  Exam({
    this.id,
    this.userId,
    this.shapterNum,
    this.ejaza,
    this.reading,
    this.status,
    this.assignedTo,
    this.note,
    this.rate,
    this.orderTime,
    this.orderDate,
    this.teacherName,
    this.studentName,
    this.fromPage,
    this.toPage,
  });

  int? id;
  int? userId;
  int? shapterNum;
  int? ejaza;
  int? reading;
  String? status;
  int? assignedTo;
  String? note;
  dynamic rate;
  String? orderTime;
  DateTime? orderDate;
  String? teacherName;
  String? studentName;
  int? fromPage;
  int? toPage;

  factory Exam.fromJson(Map<String, dynamic> json) => Exam(
        id: json["id"],
        userId: json["user_id"],
        shapterNum: json["shapter_num"],
        ejaza: json["ejaza"],
        reading: json["reading"],
        status: json["status"],
        assignedTo: json["assigned_to"],
        note: json["note"],
        rate: json["rate"],
        orderTime: json["order_time"],
        orderDate: DateTime.parse(json["order_date"]),
        teacherName: json["teacher_name"],
        studentName: json["student_name"],
        fromPage: json["from_page"],
        toPage: json["to_page"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "shapter_num": shapterNum,
        "ejaza": ejaza,
        "reading": reading,
        "status": status,
        "assigned_to": assignedTo,
        "note": note,
        "rate": rate,
        "order_time": orderTime,
        "order_date": orderDate!.toIso8601String(),
        "teacher_name": teacherName,
        "student_name": studentName,
        "from_page": fromPage,
        "to_page": toPage,
      };
}
