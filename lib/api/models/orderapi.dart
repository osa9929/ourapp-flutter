// To parse this JSON data, do
//
//     final orderapi = orderapiFromJson(jsonString);

import 'dart:convert';

Orderapi orderapiFromJson(String str) => Orderapi.fromJson(json.decode(str));

String orderapiToJson(Orderapi data) => json.encode(data.toJson());

class Orderapi {
  Orderapi({
    this.id,
    this.userId,
    this.shapterNum,
    this.fromPage,
    this.toPage,
    this.ejaza,
    this.reading,
    this.status,
    this.note,
    this.assignedTo,
    this.teacherName,
    this.orderTime,
    this.orderDate,
  });

  int? id;
  int? userId;
  int? shapterNum;
  int? fromPage;
  int? toPage;
  int? ejaza;
  int? reading;
  String? status;
  dynamic note;
  int? assignedTo;
  String? teacherName;
  String? orderTime;
  DateTime? orderDate;

  factory Orderapi.fromJson(Map<String, dynamic> json) => Orderapi(
        id: json["id"],
        userId: json["user_id"],
        shapterNum: json["shapter_num"],
        fromPage: json["from_page"],
        toPage: json["to_page"],
        ejaza: json["ejaza"],
        reading: json["reading"],
        status: json["status"],
        note: json["note"],
        assignedTo: json["assigned_to"],
        teacherName: json["teacher_name"],
        orderTime: json["order_time"],
        orderDate: DateTime.parse(json["order_date"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "shapter_num": shapterNum,
        "from_page": fromPage,
        "to_page": toPage,
        "ejaza": ejaza,
        "reading": reading,
        "status": status,
        "note": note,
        "assigned_to": assignedTo,
        "teacher_name": teacherName,
        "order_time": orderTime,
        "order_date": orderDate!.toIso8601String(),
      };
}
