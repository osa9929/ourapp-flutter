import 'package:http/http.dart' as http;
// import 'package:workspace/model/static/currentuser.dart';

class Requests {
  static String httpurl = "https://";
  static String ip = "www.cultofandroid.net/";
  // static String ip = "192.168.1.116:8000/";
  static String token = "";

  static void setIp(String ip) {
    Requests.ip = ip + ":8000/";
  }

  static Future<http.Response> postRequest(
      {required String urlRout, Object? body}) async {
    var url = Uri.parse(httpurl + ip + urlRout);
    http.Response response = await http.post(
      url,
      body: body,
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );
    return response;
  }

  static Future<http.Response> putRequest(
      {required String urlRout, body}) async {
    var url = Uri.parse(httpurl + ip + urlRout);
    http.Response response = await http.put(
      url,
      body: body,
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );
    return response;
  }

  static Future<http.Response> getRequest({required String urlRout}) async {
    var url = Uri.parse(httpurl + ip + urlRout);
    http.Response response = await http.get(
      url,
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );
    return response;
  }

  static Future<http.Response> deleteRequest({required String urlRout}) async {
    var url = Uri.parse(httpurl + ip + urlRout);
    http.Response response = await http.delete(
      url,
      headers: {"Accept": "application/json", "Authorization": "Bearer $token"},
    );
    return response;
  }
}
