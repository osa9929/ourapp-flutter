import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ourapp/api/requests.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  String? name;
  String? passWord;
  String? confirmPassWord;
  String? stuClass;
  String? groupName;
  String? phone;
  bool usedName = false;

  bool tryRgiste = false;

  String? group;
  List<String> groups = [
    "الفاروق",
    "الصديق",
    "ذي النورين",
    "المصطفى",
    "عباد الرحمن"
  ];

  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Future<bool> registCommit() async {
    try {
      setState(() {
        tryRgiste = true;
      });
      print('trying to register///////////////////////');
      http.Response response =
          await Requests.postRequest(urlRout: "api/register", body: {
        "name": "$name",
        "password": "$passWord",
        "class": "$stuClass",
        "phone": "$phone",
        "group": "$group",
        "allow_shapter": "[]",
      });

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 201) {
        print('register done///////////////////////////');
        return true;
      } else if (response.statusCode == 422) {
        usedName = true;
        validatedata();
      }
    } catch (e) {
      // ignore: avoid_print
      print(e);
    }
    setState(() {
      tryRgiste = false;
    });
    return false;
  }

  Future<void> registeResult() async {
    try {
      setState(() {
        tryRgiste = true;
      });
      bool isRegister = await registCommit();
      print(" isRegiste    =>>   $isRegister");
      if (isRegister == true) {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
            'تم إنشاء الحساب بنجاح\nقم بتسجيل الدخول الآن',
            style: TextStyle(color: Colors.green),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(seconds: 5),
          margin: EdgeInsets.all(20),
        ));
        Navigator.of(context).pop();
      } else {
        ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
          content: Text(
            'فشل إنشاء الحساب',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(seconds: 5),
          margin: EdgeInsets.all(20),
        ));
      }
    } catch (e) {
      print(e);
    }
  }

  void validatedata() {
    final FormState? form = formKey.currentState;
    if (form!.validate()) {
      registeResult();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            title: const Text("إنشاء حساب"),
            centerTitle: true,
          ),
          body: Center(
            child: SizedBox(
              width: 500,
              child: Form(
                key: formKey,
                child: ListView(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  children: [
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: SizedBox(
                            height: MediaQuery.of(context).size.height / 5,
                            child: Image.asset(
                              "assets/images/userImage.jpg",
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ),
                    ),
                    buildTextField(
                      label: "اسم المستخدم",
                      icon: Icons.person,
                      validator: (value) =>
                          value == null || value.isEmpty || value == ""
                              ? 'لايمكن ان يكون فارغاً'
                              : usedName
                                  ? 'الاسم مستخدم سابقا, أدخل اسم مختلف'
                                  : null,
                      onChanged: (val) {
                        name = val;
                      },
                    ),
                    buildTextField(
                      label: "كلمة المرور",
                      icon: Icons.lock,
                      obscureText: true,
                      validator: (value) =>
                          value == null || value.isEmpty || value == ""
                              ? 'لايمكن ان يكون فارغاً'
                              : null,
                      onChanged: (val) {
                        passWord = val;
                      },
                    ),
                    buildTextField(
                      label: "تأكيد كلمة المرور",
                      icon: Icons.lock,
                      obscureText: true,
                      validator: (value) {
                        if (passWord != confirmPassWord) {
                          return 'لا يوجد تطابق';
                        }
                        return null;
                      },
                      onChanged: (val) {
                        confirmPassWord = val;
                      },
                    ),
                    buildTextField(
                      label: "الصف",
                      icon: Icons.layers,
                      onChanged: (val) {
                        stuClass = val;
                      },
                    ),
                    buildTextField(
                      label: "رقم الهاتف",
                      icon: Icons.phone,
                      keyboardType: TextInputType.number,
                      onChanged: (val) {
                        phone = val;
                      },
                    ),
                    buildDropDownMenu(
                      items: groups,
                      onChange: (val) {
                        setState(() {
                          group = val;
                        });
                      },
                      hint: "الحلقة",
                      selected: group,
                    ),
                    const SizedBox(
                      height: 50,
                    ),
                    Center(
                      child: tryRgiste == true
                          ? const CircularProgressIndicator()
                          : ElevatedButton(
                              child: const Text(
                                "إنشاء الحساب",
                              ),
                              onPressed: () {
                                validatedata();
                              },
                            ),
                    )
                  ],
                ),
              ),
            ),
          )),
    );
  }

  Widget buildTextField({
    required String label,
    required IconData icon,
    required Function(dynamic)? onChanged,
    String? Function(String?)? validator,
    bool obscureText = false,
    TextInputType keyboardType = TextInputType.text,
  }) =>
      Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: TextFormField(
          validator: validator,
          keyboardType: keyboardType,
          obscureText: obscureText,
          decoration: InputDecoration(
            icon: Icon(icon),
            border: const OutlineInputBorder(),
            label: Text(label),
          ),
          onChanged: onChanged,
        ),
      );

  Widget buildDropDownMenu({
    String? hint,
    String? label,
    required List items,
    String? selected,
    Function(String?)? onChange,
  }) =>
      Container(
        padding: const EdgeInsets.symmetric(vertical: 5),
        color: const Color(0x7AFFFFFF),
        child: ButtonTheme(
          highlightColor: Colors.grey,
          alignedDropdown: true,
          child: DropdownButtonFormField(
              alignment: Alignment.bottomLeft,
              iconEnabledColor: Colors.grey,
              focusColor: Colors.grey,
              isDense: true,
              // menuMaxHeight: 300,
              value: selected,
              style: const TextStyle(color: Colors.black),
              hint: Text(
                hint ?? "",
                style: const TextStyle(
                  color: Colors.grey,
                ),
              ),
              isExpanded: true,
              dropdownColor: Colors.grey[300],
              decoration: InputDecoration(
                icon: const Icon(Icons.group),
                iconColor: Colors.grey,
                prefixIconColor: Colors.grey,
                contentPadding: const EdgeInsets.all(15),
                enabled: !(onChange == null),
                enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(color: Colors.grey)),
                disabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(color: Colors.grey)),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(color: Colors.grey)),
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                    borderSide: const BorderSide(color: Colors.grey)),
                labelText: label,
                labelStyle: TextStyle(
                    color: !(onChange == null) ? Colors.blue : Colors.grey),
              ),
              // items: items.map((e)=>buildMenuItems).toList(),
              items: items.map((e) => buildMenuItems(e, selected)).toList(),
              onChanged: onChange),
        ),
      );

  DropdownMenuItem<String> buildMenuItems(String item, String? selected) =>
      DropdownMenuItem(
        value: item,
        child: Text(
          item,
          style: const TextStyle(color: Colors.black),
        ),
      );
}
