import 'package:flutter/material.dart';

import '../static/userinfo.dart';

class UnActivatedPage extends StatelessWidget {
  const UnActivatedPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Container(
              padding: const EdgeInsets.all(20),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  image: const DecorationImage(
                      opacity: .5,
                      fit: BoxFit.cover,
                      image: NetworkImage(
                        "https://goodmorning1.com/wp-content/uploads/2020/01/2510-2.jpg",
                      )),
                  color: Colors.grey[200],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text(
                        "أهلا وسهلا",
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(
                        UserIfo.data!.name.toString(),
                        style: const TextStyle(
                            color: Colors.blue,
                            fontSize: 35,
                            fontWeight: FontWeight.bold),
                      ),
                      const Text(
                        "الحساب غير مفعل حاليا\nيرجى مراجعة المشرف المسؤول",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 20,
                        ),
                      ),
                    ],
                  ),
                ],
              ))),
    );
  }
}

// UserIfo.data