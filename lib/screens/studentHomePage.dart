import 'package:flutter/material.dart';
import 'package:ourapp/api/requests.dart';
import 'package:ourapp/widget/myDrawer.dart';
import 'package:ourapp/widget/myExamCard.dart';
import 'package:ourapp/widget/myRequestCard.dart';
import 'package:http/http.dart' as http;

import '../api/models/examapi.dart';
import '../api/models/orderapi.dart';
import '../static/userinfo.dart';

class StudentHomePage extends StatefulWidget {
  const StudentHomePage({Key? key}) : super(key: key);

  @override
  State<StudentHomePage> createState() => _StudentHomePageState();
}

class _StudentHomePageState extends State<StudentHomePage> {
  bool gettingOrderData = false;
  bool gettingExameData = false;
  bool noGettingDataError = true;
  bool isOrderOn = true;
  bool isExamOn = true;
  late Orderapi orderapi;
  late Examapi examapi;

  Widget? orderWidget;
  Widget? examWidget;

  Future<bool> getCurrentRecitationRequsts() async {
    try {
      print('try get my order data ///////////////////////////');
      http.Response recitationResponse = await Requests.getRequest(
        urlRout: "api/order/getMyOrder",
      );
      print('Response status: ${recitationResponse.statusCode}');
      print('my orde Response body: ${recitationResponse.body}');
      if (recitationResponse.statusCode == 200) {
        orderapi = orderapiFromJson(recitationResponse.body);
        orderWidget = MyRequestCard(order: orderapi);
        isOrderOn = false;
        return true;
      }
      if (recitationResponse.statusCode == 404) {
        isOrderOn = true;
        orderWidget = const SizedBox(
            height: 50, child: Center(child: Text("!ليس لديك طلب تسميع")));
        return true;
      }
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<bool> getCurrentexameRequsts() async {
    try {
      print('try get my exam data ///////////////////////////');
      http.Response examResponse = await Requests.getRequest(
        urlRout: "api/exam/getMyExam",
      );
      print('Response status: ${examResponse.statusCode}');
      print('exam Response body: ${examResponse.body}');
      if (examResponse.statusCode == 200) {
        examapi = examapiFromJson(examResponse.body);
        examWidget = MyExamCard(order: examapi);
        isExamOn = false;
        return true;
      }
      if (examResponse.statusCode == 404) {
        isExamOn = true;
        examWidget = const SizedBox(
            height: 50, child: Center(child: Text("!ليس لديك طلب اختبار")));
        return true;
      }
    } catch (e) {
      print(e);
    }
    return false;
  }

  Future<void> gettingDataResulte() async {
    setState(
      () {
        gettingOrderData = true;
        gettingExameData = true;
      },
    );
    await Future.delayed(const Duration(milliseconds: 300));
    Future<bool> b1 = getCurrentRecitationRequsts();
    Future<bool> b2 = getCurrentexameRequsts();
    noGettingDataError = await b1 && await b2;
    setState(
      () {
        gettingOrderData = false;
        gettingExameData = false;
      },
    );
  }

  @override
  void initState() {
    gettingDataResulte();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("صفحة الطالب"),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.refresh),
              onPressed: () {
                gettingDataResulte();
              },
            )
          ],
        ),
        drawer: UserIfo.data!.role != "student" ? null : const MyDrawer(),
        body: gettingExameData || gettingOrderData
            ? const Center(child: CircularProgressIndicator())
            : Center(
                child: SizedBox(
                  width: 500,
                  child: Column(
                    children: [
                      Expanded(
                        child: ListView(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          children: [
                            !noGettingDataError
                                ? const SizedBox(
                                    height: 50,
                                    child: Center(
                                      child: Text("!حدث خطأ ما أعد المحاولة"),
                                    ),
                                  )
                                : Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Center(
                                        child: orderWidget,
                                      ),
                                      Center(
                                        child: examWidget,
                                      ),
                                    ],
                                  ),
                          ],
                        ),
                      ),
                      const SizedBox(height: 10),
                      FittedBox(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              child: const Padding(
                                padding: EdgeInsets.all(20.0),
                                child: Text(
                                  "إنشاء طلب تسميع",
                                ),
                              ),
                              onPressed: isOrderOn
                                  ? () {
                                      Navigator.pushNamed(
                                          context, "RecitationRequestPage");
                                    }
                                  : null,
                            ),
                            const SizedBox(
                              width: 5,
                            ),
                            ElevatedButton(
                              child: const Padding(
                                padding: EdgeInsets.all(20.0),
                                child: Text(
                                  "إنشاء طلب اختبار",
                                ),
                              ),
                              onPressed: isExamOn
                                  ? () {
                                      Navigator.pushNamed(
                                          context, "ExamRquestPage");
                                    }
                                  : null,
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
      ),
    );
  }
}
