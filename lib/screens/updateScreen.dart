import "package:flutter/material.dart";
import 'package:flutter/services.dart';

class UpdateScreen extends StatelessWidget {
  const UpdateScreen({Key? key}) : super(key: key);
  final String? link = "ASASDSAD";
  @override
  Widget build(BuildContext context) {
    String? link = "123123";
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("تحديث التطبيق"),
          centerTitle: true,
        ),
        body: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Column(
                children: const [
                  Icon(
                    Icons.new_releases,
                    size: 50,
                    color: Colors.green,
                  ),
                  Text(
                    "إصدار جديد متوفر",
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.green,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
              const Text(
                "انسخ الرابط وتوجه إلى المتصفح لتحميل أخر إصدار من التطبيق",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey,
                    fontWeight: FontWeight.bold),
              ),
              ElevatedButton(
                  onPressed: () {
                    Clipboard.setData(ClipboardData(text: link));
                  },
                  child: const Text("نسخ الرابط"))
            ],
          ),
        ),
      ),
    );
  }
}
