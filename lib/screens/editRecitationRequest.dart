import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import '../api/models/orderapi.dart';
import '../api/requests.dart';

class EditRecitationRequest extends StatefulWidget {
  const EditRecitationRequest({Key? key, required this.order})
      : super(key: key);
  final Orderapi order;

  @override
  State<EditRecitationRequest> createState() => _EditRecitationRequestState();
}

class _EditRecitationRequestState extends State<EditRecitationRequest> {
  DateTime selectedDate = DateTime.now();
  dynamic timeOption;
  late String shapterNum;
  late String startPage;
  late String endPage;
  bool? isEjaza = false;
  bool? isReading = false;
  bool trySend = false;
  int i = 1;

  @override
  void initState() {
    super.initState();
    shapterNum = widget.order.shapterNum.toString();
    startPage = widget.order.fromPage.toString();
    endPage = widget.order.toPage.toString();
    selectedDate = widget.order.orderDate!;
  }

  Future<bool?> showWarning() {
    return showDialog<bool>(
      context: context,
      builder: (BuildContext context) => Directionality(
        textDirection: TextDirection.rtl,
        child: AlertDialog(
          title: const Text(
            'إنتبه!',
            style: TextStyle(color: Colors.red),
          ),
          content: const Text('تعديل الطلب يعيده إلى حالة انتظار الإستلام'),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, true),
              child: const Text('موافق'),
            ),
            TextButton(
              onPressed: () => Navigator.pop(context, false),
              child: const Text(
                'إلغاء',
                style: TextStyle(color: Colors.red),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<bool> sendRecitationRequest() async {
    try {
      print('try send order request ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: "api/order/store_update",
        body: {
          "id": widget.order.id.toString(),
          "shapter_num": shapterNum,
          "from_page": startPage,
          "to_page": endPage,
          "ejaza": isEjaza == true ? "1" : "0",
          "reading": isReading == true ? "1" : "0",
          // "status": "Waiting",
          "order_date": selectedDate.toString(),
          "order_time": timeOption.toString(),
        },
      );
      //
      //
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 200) {
        // loginapi = loginapiFromJson(response.body);
        print('done///////////////////////////');

        return true;
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        trySend = false;
      },
    );
    return false;
  }

  Future<void> sendRecitationRequestResult() async {
    setState(
      () {
        trySend = true;
      },
    );
    bool isDone = await sendRecitationRequest();

    if (isDone == true) {
      Navigator.of(context).pop();
      Navigator.pushReplacementNamed(context, "StudentHomePage");
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشلت العملية أعد المحاولة',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(milliseconds: 1500),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (i == 1) {
      selectedDate = widget.order.orderDate!;
      timeOption = widget.order.orderTime!;
      isEjaza = widget.order.ejaza == 1 ? true : false;
      isReading = widget.order.reading == 1 ? true : false;
      i++;
    }
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("تعديل طلب التسميع"),
          centerTitle: true,
        ),
        body: Center(
          child: SizedBox(
            width: 500,
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              children: [
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("أدخل معلومات الطلب :"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextFormField(
                    initialValue: widget.order.shapterNum.toString(),
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.book_outlined),
                      border: OutlineInputBorder(),
                      label: Text("رقم الجزء"),
                    ),
                    onChanged: (val) {
                      shapterNum = val;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextFormField(
                    initialValue: widget.order.fromPage.toString(),
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.file_copy_outlined),
                      border: OutlineInputBorder(),
                      label: Text("من الصفحة"),
                    ),
                    onChanged: (val) {
                      startPage = val;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextFormField(
                    initialValue: widget.order.toPage.toString(),
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.file_copy_outlined),
                      border: OutlineInputBorder(),
                      label: Text("إلى الصفحة"),
                    ),
                    onChanged: (val) {
                      endPage = val;
                    },
                  ),
                ),
                // CheckboxListTile(
                //     contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                //     title: const Text("إجازة ؟ "),
                //     value: isEjaza,
                //     controlAffinity: ListTileControlAffinity.leading,
                //     onChanged: (val) {
                //       setState(() {
                //         isEjaza = val;
                //         if (val == true) {
                //           isReading = !val!;
                //         }
                //       });
                //     }),
                CheckboxListTile(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                    title: const Text("إقرآء"),
                    value: isReading,
                    controlAffinity: ListTileControlAffinity.leading,
                    onChanged: (val) {
                      setState(() {
                        isReading = val;
                        if (val == true) {
                          isEjaza = !val!;
                        }
                      });
                    }),
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("اختر التاريخ والوقت :"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          _selectDate(context);
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(Icons.calendar_today_outlined),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                            child: Text(
                              "${selectedDate.year.toString()} / ${selectedDate.month.toString()} / ${selectedDate.day.toString()}  ",
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                timeOptionwidget(
                  "بعد الفجر",
                  "بعد الفجر",
                ),
                timeOptionwidget(
                  "بعد الظهر",
                  "بعد الظهر",
                ),
                timeOptionwidget(
                  "بعد العصر",
                  "بعد العصر",
                ),
                timeOptionwidget(
                  "بين المغرب و العشاء",
                  "بين المغرب و العشاء",
                ),
                timeOptionwidget(
                  "بعد العشاء",
                  "بعد العشاء",
                ),
                Center(
                    child: trySend
                        ? const CircularProgressIndicator()
                        : ElevatedButton(
                            onPressed: () async {
                              bool? isok = await showWarning();
                              if (isok == true) {
                                sendRecitationRequestResult();
                              }
                            },
                            child: const Text("تعديل الطلب")))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget timeOptionwidget(
    String title,
    String value,
  ) =>
      RadioListTile(
        title: Text(title),
        value: value,
        groupValue: timeOption,
        onChanged: (value) {
          setState(() {
            timeOption = value;
          });
        },
      );

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        const Duration(days: 7),
      ),
    );
    if (selected != null && selected != selectedDate) {
      setState(() {
        selectedDate = selected;
      });
    }
  }
}
