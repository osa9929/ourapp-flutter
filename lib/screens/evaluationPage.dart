import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../api/requests.dart';
import '../static/userinfo.dart';
import '../widget/acceptedRequestCard.dart';

// class CurrentOrder {
//   static Exam order = Exam();
// }

class EvaluationPage extends StatefulWidget {
  const EvaluationPage({Key? key}) : super(key: key);

  @override
  State<EvaluationPage> createState() => _EvaluationPageState();
}

class _EvaluationPageState extends State<EvaluationPage> {
  String? acceptedORrejected;
  int rate = 0;
  String? note;

  bool trySend = false;

  Future<bool> sendEvaluation() async {
    try {
      print('try  ///////////////////////////');
      http.Response response = await Requests.postRequest(
          urlRout: CurrentOrder.order.fromPage == null &&
                  CurrentOrder.order.toPage == null
              ? "api/teacher/examEvaluation"
              : "api/teacher/orderEvaluation",
          body: {
            "id": "${CurrentOrder.order.id}",
            "status": acceptedORrejected.toString(),
            "rate": "$rate",
            "note": note.toString()
          });

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 201) {
        return true;
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  Future<void> evaluationResult() async {
    setState(() {
      trySend = true;
    });
    bool isDone = await sendEvaluation();

    setState(() {
      trySend = false;
    });

    if (isDone == true) {
      Navigator.pop(context);
      Navigator.pushReplacementNamed(context, "AcceptedRequestsPage");
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشلت العملية أعد المحاولة',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(seconds: 2),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("صفحة التقييم"),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Column(
            children: [
              AcceptedRequestCard(
                order: CurrentOrder.order,
                showButton: false,
              ),
              evaluationWidget()
            ],
          ),
        ),
      ),
    );
  }

  Widget evaluationWidget() {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5),
      decoration: BoxDecoration(
          color: Colors.grey[300], borderRadius: BorderRadius.circular(10)),
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          const Text(
            "التقييم",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          const Divider(thickness: 1),
          acceptedOrRejected(),
          rateWidget(),
          noteWidget(),
          Center(
            child: Padding(
                padding: const EdgeInsets.all(10),
                child: trySend
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        onPressed: () {
                          evaluationResult();
                        },
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: const [
                            Text("إرسال التقييم "),
                            Icon(
                              Icons.send,
                              size: 15,
                            ),
                          ],
                        ))),
          )
        ],
      ),
    );
  }

  Widget acceptedOrRejected() {
    return Row(
      children: [
        Expanded(
          child: Center(
            child: RadioListTile<String>(
              value: "Accepted",
              groupValue: acceptedORrejected,
              title: const Text("ناجح"),
              onChanged: (val) {
                setState(() {
                  acceptedORrejected = val;
                });
              },
            ),
          ),
        ),
        Expanded(
          child: Center(
            child: RadioListTile<String>(
              value: "Refused",
              groupValue: acceptedORrejected,
              title: const Text("راسب"),
              onChanged: (val) {
                setState(() {
                  acceptedORrejected = val;
                });
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget rateWidget() {
    return Row(
      children: [
        Expanded(
          flex: 3,
          child: Slider(
            min: 0,
            max: 100,
            value: rate.toDouble(),
            onChanged: (val) {
              setState(() {
                rate = val.toInt();
              });
            },
          ),
        ),
        Expanded(flex: 1, child: Text("$rate %"))
      ],
    );
  }

  Widget noteWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: TextField(
        minLines: 1,
        maxLines: 20,
        keyboardType: TextInputType.multiline,
        decoration: const InputDecoration(
          border: InputBorder.none,
          label: Text("ملاحظة ..."),
        ),
        onChanged: (val) {
          note = val;
        },
      ),
    );
  }
}
