import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../api/requests.dart';

class ExamRquestPage extends StatefulWidget {
  const ExamRquestPage({Key? key}) : super(key: key);

  @override
  State<ExamRquestPage> createState() => _ExamRquestPageState();
}

class _ExamRquestPageState extends State<ExamRquestPage> {
  late String shapterNum;
  DateTime selectedDate = DateTime.now();
  dynamic timeOption;
  bool? isEjaza = false;
  bool trySend = false;

  Future<bool> sendExamRequest() async {
    try {
      print('try send request ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: "api/exam/store_update",
        body: {
          "shapter_num": shapterNum,
          "ejaza": isEjaza == true ? "1" : "0",
          // "status": "Waiting",
          "order_date": selectedDate.toString(),
          "order_time": timeOption.toString(),
        },
      );
      //
      //
      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 201) {
        // loginapi = loginapiFromJson(response.body);
        print('login done///////////////////////////');

        return true;
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        trySend = false;
      },
    );

    return false;
  }

  Future<void> sendExamRequestResulte() async {
    setState(
      () {
        trySend = true;
      },
    );
    bool isDone = await sendExamRequest();

    if (isDone == true) {
      Navigator.of(context).pop();
      Navigator.pushReplacementNamed(context, "StudentHomePage");
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشلت العملية أعد المحاولة',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(milliseconds: 1500),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          title: const Text("إنشاء طلب اختبار"),
          centerTitle: true,
        ),
        body: Center(
          child: SizedBox(
            width: 500,
            child: ListView(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              children: [
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("أدخل معلومات الطلب :"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      icon: Icon(Icons.book_outlined),
                      border: OutlineInputBorder(),
                      label: Text("رقم الجزء"),
                    ),
                    onChanged: (val) {
                      shapterNum = val;
                    },
                  ),
                ),
                // CheckboxListTile(
                //     contentPadding: const EdgeInsets.symmetric(horizontal: 0),
                //     title: const Text("إجازة ؟ "),
                //     value: isEjaza,
                //     controlAffinity: ListTileControlAffinity.leading,
                //     onChanged: (val) {
                //       setState(() {
                //         isEjaza = val;
                //       });
                //     }),
                const Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Text("اختر التاريخ والوقت :"),
                ),
                Padding(
                  padding: const EdgeInsets.all(5),
                  child: Row(
                    children: [
                      ElevatedButton(
                        onPressed: () {
                          _selectDate(context);
                        },
                        child: const Padding(
                          padding: EdgeInsets.all(5.0),
                          child: Icon(Icons.calendar_today_outlined),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Container(
                          padding: const EdgeInsets.all(12),
                          decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(5)),
                          child: Center(
                            child: Text(
                              "${selectedDate.year.toString()} / ${selectedDate.month.toString()} / ${selectedDate.day.toString()}  ",
                              textDirection: TextDirection.ltr,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                timeOptionwidget(
                  "بعد الفجر",
                  "بعد الفجر",
                ),
                timeOptionwidget(
                  "بعد الظهر",
                  "بعد الظهر",
                ),
                timeOptionwidget(
                  "بعد العصر",
                  "بعد العصر",
                ),
                timeOptionwidget(
                  "بين المغرب و العشاء",
                  "بين المغرب و العشاء",
                ),
                timeOptionwidget(
                  "بعد العشاء",
                  "بعد العشاء",
                ),
                Center(
                    child: trySend
                        ? const CircularProgressIndicator()
                        : ElevatedButton(
                            onPressed: () {
                              sendExamRequestResulte();
                            },
                            child: const Text("إرسال الطلب")))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget timeOptionwidget(
    String title,
    String value,
  ) =>
      RadioListTile(
        title: Text(title),
        value: value,
        groupValue: timeOption,
        onChanged: (value) {
          setState(() {
            timeOption = value;
          });
        },
      );

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: selectedDate,
      firstDate: DateTime.now(),
      lastDate: DateTime.now().add(
        const Duration(days: 7),
      ),
    );
    if (selected != null && selected != selectedDate) {
      setState(() {
        selectedDate = selected;
      });
    }
  }
}
