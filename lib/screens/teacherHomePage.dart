import 'package:flutter/material.dart';
import 'package:ourapp/api/models/incommingRequestsapi.dart';
import 'package:ourapp/widget/incommingRequestCard.dart';
import 'package:http/http.dart' as http;
import 'package:ourapp/widget/myDrawer.dart';

import '../api/requests.dart';

class TeacherHomePage extends StatefulWidget {
  const TeacherHomePage({Key? key}) : super(key: key);

  @override
  State<TeacherHomePage> createState() => _TeacherHomePageState();
}

class _TeacherHomePageState extends State<TeacherHomePage> {
  bool gettingData = false;
  late IncommingRequestsapi incommingRequestsapi;

  Future<bool> getIncommingRequests() async {
    try {
      setState(
        () {
          gettingData = true;
        },
      );

      print('try getting data ////////////////');
      http.Response response = await Requests.getRequest(
        urlRout: "api/teacher/getAllowOrders_Exams",
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 200) {
        incommingRequestsapi = incommingRequestsapiFromJson(response.body);
        setState(
          () {
            gettingData = false;
          },
        );

        if (incommingRequestsapi.orders!.isEmpty) {
          print("orders is empty . . . . ");
        }
        if (incommingRequestsapi.exams!.isEmpty) {
          print("exams is empty . . . . ");
        }
        return true;
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        gettingData = false;
      },
    );
    return false;
  }

  @override
  void initState() {
    getIncommingRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            title: const Text("الطلبات الواردة"),
            centerTitle: true,
            actions: [
              IconButton(
                icon: const Icon(Icons.refresh),
                onPressed: () {
                  getIncommingRequests();
                },
              )
            ],
          ),
          drawer: const MyDrawer(),
          body: gettingData
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : ListView(
                  padding: const EdgeInsets.all(5),
                  children: [
                    SizedBox(
                      height: 50,
                      child: Row(
                        children: const [
                          Icon(Icons.arrow_drop_down),
                          Text("طلبات التسميع الواردة"),
                        ],
                      ),
                    ),
                    if (incommingRequestsapi.orders!.isNotEmpty)
                      for (var i = 0;
                          i < incommingRequestsapi.orders!.length;
                          i++)
                        IncommingRequsetCard(
                            order: incommingRequestsapi.orders!.elementAt(i)),
                    SizedBox(
                      height: 50,
                      child: Row(
                        children: const [
                          Icon(Icons.arrow_drop_down),
                          Text("طلبات الاختبار الواردة"),
                        ],
                      ),
                    ),
                    if (incommingRequestsapi.exams!.isNotEmpty)
                      for (var i = 0;
                          i < incommingRequestsapi.exams!.length;
                          i++)
                        IncommingRequsetCard(
                            order: incommingRequestsapi.exams!.elementAt(i))
                  ],
                )),
    );
  }
}
