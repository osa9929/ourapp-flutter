import 'package:flutter/material.dart';

import '../api/models/incommingRequestsapi.dart';
import '../api/requests.dart';
import '../widget/acceptedRequestCard.dart';
import 'package:http/http.dart' as http;

class AcceptedRequestsPage extends StatefulWidget {
  const AcceptedRequestsPage({Key? key}) : super(key: key);

  @override
  State<AcceptedRequestsPage> createState() => _AcceptedRequestsPageState();
}

class _AcceptedRequestsPageState extends State<AcceptedRequestsPage> {
  bool gettingData = false;
  late IncommingRequestsapi incommingRequestsapi;

  Future<bool> getIncommingRequests() async {
    try {
      setState(
        () {
          gettingData = true;
        },
      );

      print('try getting data ////////////////');
      http.Response response = await Requests.getRequest(
        urlRout: "api/teacher/getAllAccepted",
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');
      if (response.statusCode == 200) {
        incommingRequestsapi = incommingRequestsapiFromJson(response.body);
        setState(
          () {
            gettingData = false;
          },
        );

        if (incommingRequestsapi.orders!.isEmpty) {
          print("orders is empty . . . . ");
        }
        if (incommingRequestsapi.exams!.isEmpty) {
          print("exams is empty . . . . ");
        }
        return true;
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        gettingData = false;
      },
    );
    return false;
  }

  @override
  void initState() {
    getIncommingRequests();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.green,
            title: const Text("الطلبات المستلمة"),
            centerTitle: true,
            actions: [
              IconButton(
                icon: const Icon(Icons.refresh),
                onPressed: () {
                  getIncommingRequests();
                },
              )
            ],
          ),
          body: gettingData
              ? const Center(
                  child: CircularProgressIndicator(
                    color: Colors.green,
                  ),
                )
              : ListView(
                  padding: const EdgeInsets.all(5),
                  children: [
                    SizedBox(
                      height: 50,
                      child: Row(
                        children: const [
                          Icon(Icons.arrow_drop_down),
                          Text("طلبات التسميع السابقة"),
                        ],
                      ),
                    ),
                    if (incommingRequestsapi.orders!.isNotEmpty)
                      for (var i = 0;
                          i < incommingRequestsapi.orders!.length;
                          i++)
                        AcceptedRequestCard(
                            order: incommingRequestsapi.orders!.elementAt(i)),
                    SizedBox(
                      height: 50,
                      child: Row(
                        children: const [
                          Icon(Icons.arrow_drop_down),
                          Text("طلبات الاختبار السابقة"),
                        ],
                      ),
                    ),
                    if (incommingRequestsapi.exams!.isNotEmpty)
                      for (var i = 0;
                          i < incommingRequestsapi.exams!.length;
                          i++)
                        AcceptedRequestCard(
                            order: incommingRequestsapi.exams!.elementAt(i))
                  ],
                )),
    );
  }
}
