import 'package:ourapp/api/models/loginapi.dart';
import 'package:ourapp/api/requests.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:ourapp/static/userinfo.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool showLoginFields = false;
  String? userName;
  String? passWord;
  bool tryLogin = false;
  late final SharedPreferences prefs;
  late final Loginapi loginData;

  Future<bool> logincommit() async {
    try {
      setState(
        () {
          tryLogin = true;
        },
      );

      print('try login ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: "api/login",
        body: {"name": "$userName", "password": "$passWord"},
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      if (response.statusCode == 201) {
        print('login done///////////////////////////');
        loginData = loginapiFromJson(response.body);
        Requests.token = loginData.token;
        UserIfo.data = loginData.user;
        prefs.setString('token', loginData.token);
        return true;
      }
    } catch (e) {
      print(e);
    }
    setState(
      () {
        tryLogin = false;
      },
    );
    return false;
  }

  Future<void> loginResult() async {
    bool islogin = await logincommit();
    if (islogin == true) {
      if (loginData.user!.activation == 0) {
        Navigator.of(context).pushNamed('UnActivatedPage');
      } else if (loginData.user!.role == "student") {
        Navigator.of(context).pushReplacementNamed('StudentHomePage');
      } else {
        Navigator.of(context).pushReplacementNamed('TeacherHomePage');
      } // or StudentHomePage  TeacherHomePage
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'فشل تسجيل الدخول',
            style: TextStyle(color: Colors.red),
          ),
          behavior: SnackBarBehavior.floating,
          backgroundColor: Color(0xFFCACACA),
          duration: Duration(seconds: 2),
          margin: EdgeInsets.all(20),
        ),
      );
    }
  }

  void checkToken() async {
    prefs = await SharedPreferences.getInstance();
    final String? savedToken = prefs.getString('token');

    if (savedToken == null) {
      setState(() {
        print("don't have token");
        showLoginFields = true;
      });
    } else {
      try {
        print("have token ... check token");
        Requests.token = savedToken;

        http.Response response =
            await Requests.getRequest(urlRout: "api/isToken");
        print('Response status: ${response.statusCode}');
        print('Response body: ${response.body}');

        if (response.statusCode == 200) {
          UserIfo.data = userFromJson(response.body);
          if (UserIfo.data!.activation == 0) {
            Navigator.of(context).pushNamed('UnActivatedPage');
          } else if (UserIfo.data!.role == "student") {
            Navigator.of(context).pushReplacementNamed('StudentHomePage');
          } else {
            Navigator.of(context).pushReplacementNamed('TeacherHomePage');
          }
        } else {
          setState(() {
            showLoginFields = true;
          });
        }
      } catch (e) {
        setState(() {
          print("check token error");
          showLoginFields = true;
        });
      }
    }
  }

  @override
  void initState() {
    print("init state");
    checkToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        body: showLoginFields == false
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : SingleChildScrollView(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    child: SizedBox(
                      width: 500,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          const SizedBox(
                            height: 20,
                          ),
                          Center(
                            child: Padding(
                              padding: const EdgeInsets.all(30.0),
                              child: SizedBox(
                                height: MediaQuery.of(context).size.height / 3,
                                child: Image.asset(
                                  "assets/images/logo.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: TextFormField(
                              decoration: const InputDecoration(
                                icon: Icon(Icons.person),
                                border: OutlineInputBorder(),
                                label: Text("اسم المستخدم"),
                              ),
                              onChanged: (val) {
                                userName = val;
                              },
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 10),
                            child: TextFormField(
                              decoration: const InputDecoration(
                                icon: Icon(Icons.lock),
                                border: OutlineInputBorder(),
                                label: Text("كلمة المرور"),
                              ),
                              obscureText: true,
                              onChanged: (val) {
                                passWord = val;
                              },
                            ),
                          ),
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              const Text("لا تمتلك حساب؟"),
                              TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(
                                        context, "RegisterPage");
                                  },
                                  child: const Text("إنشاء حساب"))
                            ],
                          ),
                          tryLogin == true
                              ? const CircularProgressIndicator()
                              : Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: ElevatedButton(
                                      onPressed: () {
                                        loginResult();
                                      },
                                      child: const Text("تسجيل الدخول")),
                                ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
      ),
    );
  }
}
