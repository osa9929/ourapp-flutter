import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../api/requests.dart';

class AccountManagementPage extends StatefulWidget {
  const AccountManagementPage({Key? key}) : super(key: key);

  @override
  State<AccountManagementPage> createState() => _AccountManagementPageState();
}

class _AccountManagementPageState extends State<AccountManagementPage> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  late String trueCurrentPassWord = "";
  late String currentPassWord = "";
  late String newPassWord = "";
  late String newPassWordConfirm = "";
  bool tryUpdatePassword = false;

  Future<bool> updatePassword() async {
    try {
      setState(
        () {
          tryUpdatePassword = true;
        },
      );

      print('try update password ///////////////////////////');
      http.Response response = await Requests.postRequest(
        urlRout: "api/editPassword",
        body: {"password": currentPassWord, "newPassword": newPassWord},
      );

      setState(
        () {
          tryUpdatePassword = false;
        },
      );

      print('Response status: ${response.statusCode}');
      print('Response body: ${response.body}');

      if (response.statusCode == 200) {
        Navigator.pop(context);
      }
      if (response.statusCode == 401) {
        trueCurrentPassWord = "";
        formKey.currentState!.validate();
      }
    } catch (e) {
      print(e);
    }

    return false;
  }

  void validatedata() {
    final FormState? form = formKey.currentState;
    if (form!.validate()) {
      updatePassword();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Row(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Icon(Icons.settings),
              SizedBox(
                width: 5,
              ),
              Text("إدارة الحساب")
            ],
          ),
        ),
        body: Padding(
          padding: const EdgeInsets.all(10),
          child: Form(
            key: formKey,
            child: Column(
              children: [
                buildTextField(
                    obscureText: true,
                    label: "كلمة المرور الحالية",
                    validator: (val) {
                      if (trueCurrentPassWord != currentPassWord) {
                        return 'كلمة المرور غير صحيحة';
                      } else if (currentPassWord == "" ||
                          currentPassWord.isEmpty) {
                        return "لا يمكن ان يكون الحقل فارغا";
                      }
                      return null;
                    },
                    onChanged: (val) {
                      currentPassWord = val;
                      trueCurrentPassWord = val;
                    }),
                buildTextField(
                    obscureText: true,
                    label: "كلمة المرور الجديدة",
                    validator: (val) {
                      if (newPassWord == "" || newPassWord.isEmpty) {
                        return "لا يمكن ان يكون الحقل فارغا";
                      }
                      return null;
                    },
                    onChanged: (val) {
                      newPassWord = val;
                    }),
                buildTextField(
                    obscureText: true,
                    label: "تأكيد كلمة المرور الجديدة",
                    validator: (val) {
                      if (newPassWord != newPassWordConfirm) {
                        return 'لا يوجد تطابق';
                      }
                      return null;
                    },
                    onChanged: (val) {
                      newPassWordConfirm = val;
                    }),
                tryUpdatePassword
                    ? const CircularProgressIndicator()
                    : ElevatedButton(
                        onPressed: () {
                          validatedata();
                        },
                        child: const Center(child: Text('تحديث كلمة المرور')))
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildTextField({
    required String label,
    required Function(dynamic)? onChanged,
    String? Function(String?)? validator,
    bool obscureText = false,
    TextInputType keyboardType = TextInputType.text,
  }) =>
      Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 5,
        ),
        child: TextFormField(
          validator: validator,
          keyboardType: keyboardType,
          obscureText: obscureText,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            label: Text(label),
          ),
          onChanged: onChanged,
        ),
      );
}
