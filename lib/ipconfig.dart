import 'package:flutter/material.dart';
import 'package:ourapp/api/requests.dart';

class IpConfig extends StatelessWidget {
  IpConfig({Key? key}) : super(key: key);

  String? ip;
  final TextEditingController? textcontroller = TextEditingController(
    text: "192.168.",
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const Text("IP Configration"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    child: Center(
                      child: TextFormField(
                        textAlign: TextAlign.center,
                        controller: textcontroller,
                        onChanged: (val) {
                          ip = val;
                          print(ip);
                        },
                      ),
                    ),
                  ),
                ],
              ),
              ElevatedButton(
                child: const Text("next >"),
                onPressed: () {
                  ip == null ? ip = "" : null;
                  Requests.setIp(ip!);

                  Navigator.pushReplacementNamed(context, "LoginPage");
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
